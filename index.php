<?php

require __DIR__ . '/vendor/autoload.php';

use App\Database\DBRegistry;
use App\DataConfig;
use App\DataLoader;

$timeStart = microtime(true);

$registry = DBRegistry::getInstance();

$loader = new DataLoader();

$bsExpectedNumRef = $loader->loadAllBlobStorage();
$bsActualNumRef = [];
$bsRelations = $loader->loadAllRelations(false);

$results = [
    'zeroCount' => [],
    'diffCount' => [],
    'notExists' => [],
];

foreach ($bsRelations as $tableName => $records) {
    foreach ($records as $record) {
        $primaryName = DataConfig::REL_TABLES_SCHEMA[$tableName]['primary'] ?? $tableName . DataConfig::DEFAULT_ID_SUFFIX;
        $primaryValue = $record[$primaryName];
        $fields = DataConfig::REL_TABLES_SCHEMA[$tableName]['fields'];

        foreach ($fields as $field) {
            $blobStorageId = $record[$field];

            if (!array_key_exists($blobStorageId, $bsExpectedNumRef)) {
                $results['notExists'][$blobStorageId][] = [$tableName . '.' . $primaryName => $primaryValue];
                continue;
            }

            if (array_key_exists($blobStorageId, $bsActualNumRef)) {
                $bsActualNumRef[$blobStorageId] += 1;
            } else {
                $bsActualNumRef[$blobStorageId] = 1;
            }
        }
    }
}

foreach ($bsActualNumRef as $blobStorageId => $actualCount) {
    $expectedCount = $bsExpectedNumRef[$blobStorageId];

    if ($actualCount === 0 && $expectedCount === 0) {
        $results['zeroCount'][] = $blobStorageId;
    } elseif ($actualCount !== $expectedCount) {
        $results['diffCount'][$blobStorageId] = [
            'expected' => $expectedCount,
            'actual' => $actualCount,
            'toDelete' => $actualCount === 0
        ];
    }

    unset($bsExpectedNumRef[$blobStorageId]);
}

$results['zeroCount'] = array_merge($results['zeroCount'], array_keys($bsExpectedNumRef));

$fp = fopen('results.json', 'w');
fwrite($fp, json_encode($results, JSON_PRETTY_PRINT));
fclose($fp);

echo 'Execution finish, during ' . (microtime(true) - $timeStart) . ' seconds' . PHP_EOL;
