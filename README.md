## Proton Test

### Requirements

- PHP 7.0+
- Install [Composer](https://getcomposer.org/)
- Enable [PDO extension](https://www.php.net/manual/en/ref.pdo-mysql.php)
- PCNTL and Shmop functions on PHP version

### Installation

* Clone the source code
    
    
    git clone https://gitlab.com/Ashk2a/proton-test.git
    
* Install required dependencies with composer

    
    composer install
    
 
### Running 
   
* Run the mono thread version or the parallel version 

    
    php index.php
    php parallel.php
 
 The output is `results.json` for the mono thread solution and `result_multi_threads` for the parallel solution.
 
 Example of output format :
 
 ```json
{
    "zeroCount": [
        1896
    ],
    "diffCount": {
        "1899": {
            "expected": 0,
            "actual": 1,
            "toDelete": false
        }
    },
    "notExists": {
        "12121241": [
            {
                "Attachment.AttachmentID": 121096
            }
        ]
    }
}
```

- **zeroCount** : Contains all `BlobStorage.BlobStorageID` with `BlobStorage.NumReferences` = 0 and with no real existing references find by the algorithm.

- **diffCount** : The key is the `BlobStorage.BlobStorageID`. The algorithm detects a difference between the value stocked and the number of real references.<br>The field `expected` is the value of `BlobStorage.NumReferences` and the field `actual` is the number of references find by the algorithm.<br>The field `toDelete` is true if `actual` is equal to 0.

- **notExists** : The key is the `BlobStorage.BlobStorageID`.<br> This one contains all resources which use a non existing `BlobStorage.BlobStorageID`.
