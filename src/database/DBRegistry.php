<?php


namespace App\Database;


class DBRegistry
{
    const CONFIG = [
        'global' => [
            'host' => 'localhost',
            'db_name' => 'ProtonMailGlobal',
            'port' => 3306,
            'user' => 'proton',
            'password' => 'proton'
        ],
        'shard' => [
            'host' => 'localhost',
            'db_name' => 'ProtonMailShard',
            'port' => 3306,
            'user' => 'proton',
            'password' => 'proton'
        ]
    ];

    /**
     * @var self
     */
    private static $_instance = null;

    /**
     * @var array
     */
    private $connections;

    /**
     * DBRegistry constructor.
     */
    private function __construct()
    {
        foreach (self::CONFIG as $connectionName => $connectionInfo) {
            $this->connections[$connectionName] = new DBConnection($connectionInfo);
        }
    }

    /**
     * @param string $connectionName
     * @return DBConnection|null
     */
    public function getConnection(string $connectionName): ?DBConnection
    {
        return $this->connections[$connectionName];
    }

    /**
     * @return DBRegistry
     */
    public static function getInstance() {
        if (self::$_instance) {
            return self::$_instance;
        }

        return new DBRegistry();
    }
}
